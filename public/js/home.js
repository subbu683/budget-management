$(function(){  
  $(".addCategory").click(function(){
     $(".add_category_modal").addClass("is-active");
     $(".modal-background, #close").click(function(){
      $(".add_category_modal").removeClass("is-active");
     });
  });
  $(".addUser").click(function(){
     $(".add_user_modal").addClass("is-active");
     $(".modal-background, #close").click(function(){
      $(".add_user_modal").removeClass("is-active");
     });
  });
  $("#auth_checkbox").click(function(){
    $("#auth_hide").toggle(500);
  });
  $("#addButton").click(function(event){
      var text = $("#addCategory").val();
      if(text == '')
      {
        $("#addCat_fail").html("Please fill the form");
      }else{
      $(".add_category_modal").removeClass('is-active');
     $.post('addCategory',{'text':text,'_token':$('input[name=_token]').val()}, function(data){
           $("#addCategory_ajax").load(location.href + " #addCategory_ajax");
      });
      }
  });
    $("#addUser_button").click(function(event){
      var text = $("#addUser").val();
      if(text == '')
      {
           $("#addUser_fail").html("Please fill the form");
      }else{

      $(".add_user_modal").removeClass('is-active');
      $.post('addUser',{'friend':text,'_token':$('input[name=_token]').val()}, function(data){
           $("#addUser_ajax").load(location.href + " #addUser_ajax");
      });
      }
  });
     $(".navbar-burger").click(function(){
          $(this).toggleClass("is-active");
          $(".navbar-menu").toggle();
    });
});