$(function(){
  $("#close").click(function(){
    $("#reglog_div,#register_div").hide(1100,'easeOutQuad');
    $("#signin_div").hide(1100,'easeOutQuad');
    $(".details").show(1000,'easeInOutQuad');
    

   
  });
  $("#Close").click(function(){
    $("#signin_div").hide(1100,'easeOutQuad');
    $(".details").show(1000,'easeInOutQuad');


   
  });
  $(".signup_button").click(function(){
    $("#signin_div").hide();

    $(".details").hide(1000,'easeInOutQuad');
    // $(this).attr('disabled','disabled');
    $("#reglog_div,#register_div").show(1100,'easeOutQuad');
  });
  $(".signin_button,#is-link").click(function(){
    $("#reglog_div").hide();

    $(".details").hide(1000,'easeInOutQuad');
    // $(this).attr('disabled','disabled');
    $("#signin_div").show(1100,'easeOutQuad');
  });
  
  $("#login_card,#is-link").click(function(){
     $("#register_div").hide();
     $("#login_div").show();
  });
  $("#register_card").click(function(){
     $("#login_div").hide();
     $("#register_div").show();
  });
  $("#nickname").on("change",function(){
          var nickname=$("#nickname").val();
          if (!/[^a-zA-Z]/.test(nickname)){
          if(nickname.length > 1)
          {
            $("#nickname").removeClass("is-danger");
            $("#nickname").addClass("is-success");
            $("#nick_check").addClass("fa-check");
            $("#nick_success").html("");
            $("#button_reg").removeAttr("disabled");
          }
          else
          {
            $("#button_reg").attr("disabled","disabled");
            $(this).addClass("is-danger");
            $("#nick_check").removeClass("fa-check");
            $("#nick_success").html("Nickname should have atleast 2 characters");
          }
          }
          else
          {
            $("#button_reg").attr("disabled","disabled");

            $("#nick_success").html("Nickname should have only character string");
          }
  });
    $("#username").on("change",function(){
          var username=$("#username").val();
          if (/^[a-zA-Z0-9_-]{3,16}$/.test(username)){
          if(username.length > 4)
          {

            $("#username").removeClass("is-danger");
            $("#username").addClass("is-success");
            $("#user_check").addClass("fa-check");
            $("#user_success").html("");
            $("#button_reg").removeAttr("disabled");
          }
          else
          {
            $("#button_reg").attr("disabled","disabled");
            $(this).addClass("is-danger");
            $("#user_check").removeClass("fa-check");
            $("#user_success").html("Username should have atleast 5 characters");
          }
          }
          else
          {
            $("#button_reg").attr("disabled","disabled");

            $("#user_success").html("Username should be combination of character string , numbers , no spaces and without special chars");
          }
  });

   $("#password").on("change",function(){
          var password=$("#password").val();
          if(password.length > 5)
          {
            $("#password").removeClass("is-danger");
            $("#password").addClass("is-success");
            $("#pass_check").addClass("fa-check");
            $("#pass_success").html("");
            $("#button_reg").removeAttr("disabled");
          }
          else
          {
            $("#button_reg").attr("disabled","disabled");
            $(this).addClass("is-danger");
            $("#pass_check").removeClass("fa-check");
            $("#pass_success").html("Password must be atleast 6 characters");
          }
          }); 
    $(".navbar-burger").click(function(){
          $(this).toggleClass("is-active");
          $(".navbar-menu").toggle();
    });       

});