    var snippet = 
     '<article class="message is-primary">'+
      '<div class="message-body" id="show">'+
      '<table  class="table is-striped is-fullwidth">'+
        '<thead>'+
          '<tr>'+
            '<th>Date</th>'+
            '<th>Category</th>'+
            '<th>Paidby</th>'+
            '<th>Amount</th>'+
            '<th>Description</th>'+
          '</tr>'+
        '</thead>'+
        '<tbody id="table_body" >'+          
        '</tbody>'+
      '</table>'
      '</div>'+
      '<article>'+     
      '</div>'
$(function() {
 $("#byuser_tile").on('click',function(){
   $("#accordian_category").hide(500);
   $("#accordian_date").hide(500);
     $("#accordian_user").show(500);
     event.stopPropagation();
   }); 
 $(document).on('click',function(e){
   $("#accordian_user").hide(500);
   $("#accordian_category").hide(500);
   $("#accordian_date").hide(500);
 });
 $("#bycategory_tile").click(function(){
   $("#accordian_user").hide(500);
   $("#accordian_date").hide(500);
   $("#accordian_category").show(500);
   event.stopPropagation();
 });  
 $("#bydate_tile").click(function(){
   $("#accordian_user").hide(500);
   $("#accordian_category").hide(500);
   $("#accordian_date").show(500);
   event.stopPropagation();
 });
 $("#bydate_op").click(function(){
  $("#byyear_re").hide(500);
  $(this).addClass('is-active');
  $("#bymonth_op").removeClass("is-active");
  $("#byyear_op").removeClass("is-active");
  $("#bymonth_re").hide(500);
  $("#bydate_re").show(500);
});
 $("#bymonth_op").click(function(){
  $("#bydate_re").hide(500);
  $("#bydate_op").removeClass("is-active");
  $("#byyear_op").removeClass("is-active");
  $(this).addClass("is-active");
  $("#byyear_re").hide(500);
  $("#bymonth_re").show(500);
});
 $("#byyear_op").click(function(){
  $("#bymonth_re").hide(500);
  $("#bydate_re").hide(500);
  $("#bymonth_op").removeClass("is-active");
  $("#bydate_op").removeClass("is-active");
  $(this).addClass("is-active");
  $("#byyear_re").show(500);
});
 $.ajaxSetup({
  headers: {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
  }
});
$("#user_submit").on('click',function(e){
  $(this).attr("disabled","disabled"); 
  e.preventDefault();
  var name = $("#by_user").val();
     $.post('/getUserTransactions',{'name':name},function(data){  
      // console.log(data);
    
      var $html = $(snippet);
      $("#showCase").html($html);
      $.each(data,function(key){
       var category = data[key].category.category;
       var amount = data[key].amount;
       var paidby = data[key].users.username;
       var date = data[key].dateOfEntry;
       var description = data[key].description;
       var name = data[key].users.username;
     $('#table_body').append("<tr><td>"+date+"</td><td>"+category+"</td><td>"+paidby+"</td><td>"+amount+"</td><td>"+description+"</td><tr>");   
     });
    });  
 });
$("#by_user,#by_category,#from_date,#to_date,#by_month,#by_year,#only_year").on('click',function(){
  $("#user_submit,#category_submit,#by_date_submit,#by_month_submit,#by_year_submit").removeAttr("disabled");
});

$("#category_submit").on('click',function(e){
  e.preventDefault();
  $(this).attr("disabled","disabled");
  var name = $("#by_category").val();
     $.post('/getCategoryTransactions',{'name':name},function(data){
     
      var $html = $(snippet);
      $("#showCase").html($html);
      $.each(data,function(key){
       var date = data[key].dateOfEntry;
       var category = data[key].category.category;
       var paidby = data[key].users.username;
       var amount = data[key].amount;
       var description = data[key].description;
       $('#table_body').append("<tr><td>"+date+"</td><td>"+category+"</td><td>"+paidby+"</td><td>"+amount+"</td><td>"+description+"</td><tr>");   
     });
    });   
 });
$("#by_date_submit").on('click',function(){
  $(this).attr("disabled","disabled");
      var  from_date  = $("#from_date").val();
       var  to_date  = $("#to_date").val();
       if(to_date == '' && from_date =='')
       {
        console.log("yeah");
       }
       else{
       $.post('getBydateTransactions',{'from':from_date,'to':to_date},function(data){  
       console.log(data);  
      var $html = $(snippet);
      $("#showCase").html($html);
      $.each(data,function(key){
         var date = data[key].dateOfEntry;
       var category = data[key].category.category;
       var paidby = data[key].users.username;
       var amount = data[key].amount;
       var description = data[key].description;
        $('#table_body').append("<tr><td>"+date+"</td><td>"+category+"</td><td>"+paidby+"</td><td>"+amount+"</td><td>"+description+"</td><tr>");   
     });
       }); 
       }
});
$("#by_month_submit").on('click',function(){
  $(this).attr("disabled","disabled");
      var  month  = $("#by_month").val();
       var  year  = $("#by_year").val();
       var from = '01';
       var to = '31';
     var fromdate = [year,month,from].join('-');
      var todate = [year,month,to].join('-');
       if(month == 'Select month' && year =='Select year')
       {
        console.log("yeah");
       }
       else{
       $.post('getBymonthTransactions',{'from':fromdate,'to':todate},function(data){
        console.log(data);   
      var $html = $(snippet);
      $("#showCase").html($html);
      $.each(data,function(key){
         var date = data[key].dateOfEntry;
       var category = data[key].category.category;
       var paidby = data[key].users.username;
       var amount = data[key].amount;
       var description = data[key].description;
         $('#table_body').append("<tr><td>"+date+"</td><td>"+category+"</td><td>"+paidby+"</td><td>"+amount+"</td><td>"+description+"</td><tr>");   
     });
       }); 
       }
});
$("#by_year_submit").on('click',function(){
  $(this).attr("disabled","disabled");
       var  year  = $("#only_year").val();
       var from_month = '01';
       var to_month = '12';
       var from = '01';
       var to = '31';
     var fromdate = [year,from_month,from].join('-');
      var todate = [year,to_month,to].join('-');
       if( year =='Select year')
       {
        console.log("yeah");
       }
       else{

       $.post('getBymonthTransactions',{'from':fromdate,'to':todate},function(data){
       console.log(data);      
      var $html = $(snippet);
      $("#showCase").html($html);
      $.each(data,function(key){
       var date = data[key].dateOfEntry;
       var category = data[key].category.category;
       var paidby = data[key].users.username;
       var amount = data[key].amount;
       var description = data[key].description;
        $('#table_body').append("<tr><td>"+date+"</td><td>"+category+"</td><td>"+paidby+"</td><td>"+amount+"</td><td>"+description+"</td><tr>");   
     });
       }); 
       }
});
 $(".navbar-burger").click(function(){
          $(this).toggleClass("is-active");
          $(".navbar-menu").toggle();
    });

$(".edit").click(function(){
  $(".is_edit").addClass('is-active');
  $(".modal-close,.modal-background").click(function(){
  $(".is_edit").removeClass('is-active');

  })
})

});