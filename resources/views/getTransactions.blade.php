<!doctype html>
<html lang="{{ app()->getLocale() }}">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="csrf-token" content="{{ csrf_token() }}">
      <title>Budma</title>
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
      <link rel="stylesheet" type="text/css" href="css/bulma.css">
      <link rel="stylesheet" type="text/css" href="css/jquery-ui.min.css">
      <script src="js/jquery.js"></script>
      <script src="js/jquery-ui.min.js"></script>
      <script src="js/getTransactions.js"></script>
      <style type="text/css">
     
         #msg_notrans{
            margin-top: 100px;
         }
      </style>
   </head>
   <body>
    <div class="container is-fullhd ">
   <div class="columns">
      <div class="column">

         <nav class="navbar is-transparent is-light" role="navigation" aria-label="main navigation" >
            <div class="navbar-brand">
               <a href="/home" class="nav-item">
                  <img src="/storage/images/budma_logo1.png"  height="100" width="132" alt="BUDMA logo">
               </a>

               <div class="burger navbar-burger">
                  <span></span>
                  <span></span>
                  <span></span>
               </div>
            </div>
            <div class="navbar-menu">
               <div class="navbar-end">
                  <div class="navbar-item">
                     <div class="field is-grouped">
                        <p class="control">
                           <a class="button is-primary" href="/logout">
                              <span>logout</span>
                           </a>
                        </p>
                     </div>

                  </div>
               </div>
            </div>
         </nav>

      </div>
   </div>
   </div>
      <br>
      <div class="container">
   <div class="navbar-menu">
      
      <div class="columns">
         <div class="column">
            <div class="tile is-parent"  id="byuser_tile">
               <article class="tile is-child notification is-success">
                  <div class="content">
                     <p class="title">By user <i class="fa fa-user is-pulled-right"></i> </p>
                     <p class="subtitle is-small clickme_user" id="clickme_user">Click me!I'll give your transactions by user</p>
                     <div class="content">
                        <div id="accordian_user" style="display: none;">
                           <form>
                              {{csrf_field()}}
                              <div class="field" >
                                 <label class="label">Paidby</label>
                                 <div class="control">
                                    <div class="select" id="addUser_ajax">
                                       <select name="paidby" id="by_user">
                                          @for ($i=0; $i< count($users);$i++)
                                          <option value="{{$users[$i]->id}}">{{$users[$i]->username}}</option>
                                          @endfor
                                       </select>
                                    </div>
                                 </div>
                              </div>
                              <div class="field is-grouped">
                                 <div class="control">
                                    <button class="button is-primary" id="user_submit">Submit</button>
                                 </div>
                              </div>
                           </form>
                        </div>
                     </div>
                  </div>
               </article>
            </div>
         </div>
         <div class="column">
            <div class="tile is-parent" id="bycategory_tile">
               <article class="tile is-child notification is-success">
                  <div class="content">
                     <p class="title">By category <i class="fa fa-list is-pulled-right"></i></p>
                     <p class="subtitle is-small" id="clickme_cat">Click me! I'll give your transactions by category</p>
                     <div class="content" >
                        <div id="accordian_category" style="display: none;">
                           <div class="field" >
                              <label class="label">Category </label>
                              <div class="control">
                                 <div class="select" id="addCategory_ajax">
                                    <select name="category" id="by_category">
                                       @for ($i=0 ; $i< count($categories);$i++)
                                       <option value="{{$categories[$i]->id}}">{{$categories[$i]->category}}</option>
                                       @endfor
                                    </select>
                                 </div>
                              </div>
                           </div>
                           <div class="control">
                              <a class="button is-primary" id="category_submit" >Submit</a>
                           </div>
                        </div>
                     </div>
                  </div>
               </article>
            </div>
         </div>
         <div class="column">
            <div class="tile is-parent" id="bydate_tile">
               <article class="tile is-child notification is-success">
                  <div class="content">
                     <p class="title">By date <i class="fa fa-calendar is-pulled-right"></i></p>
                     <p class="subtitle is-small" id="clickme_date">Click me! I'll give your transactions by date</p>
                     <div class="content">
                        <div id="accordian_date" style="display:none;">
                           <nav class="panel">
                              <p class="panel-tabs" >
                                 <a id="bydate_op" class="is-active">By date</a>
                                 <a id="bymonth_op">By month</a>
                                 <a id="byyear_op">By year</a>
                              </p>
                              <div  id="bydate_re">
                                 <div class="field is-horizontal">
                                    <div class="field-label is-normal">
                                       <label class="label">From</label>
                                    </div>
                                    <div class="field-body">
                                       <div class="field">
                                          <p class="control is-expanded has-icons-left">
                                             <input class="button" id="from_date" type="date" name="from_date" >
                                          </p>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="field is-horizontal">
                                    <div class="field-label is-normal">
                                       <label class="label">To</label>
                                    </div>
                                    <div class="field-body">
                                       <div class="field">
                                          <p class="control is-expanded has-icons-left">
                                             <input class="button" id="to_date" type="date" name="to_date" >
                                          </p>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="field is-horizontal">
                                    <div class="field-label">
                                    </div>
                                    <div class="field-body">
                                       <div class="field">
                                          <div class="control">
                                             <button id="by_date_submit" class="button is-primary">
                                             Submit
                                             </button>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div style="display: none;" id="bymonth_re">
                                 <div class="field is-horizontal">
                                    <div class="field-label is-normal">
                                       <label class="label">Month</label>
                                    </div>
                                    <div class="field-body">
                                       <div class="field is-narrow">
                                          <div class="control">
                                             <div class="select is-fullwidth">
                                                <select name="by_month" id="by_month" >
                                                   <option>Select month</option>
                                                   <option value="1">Jan</option>
                                                   <option value="2">Feb</option>
                                                   <option value="3">Mar</option>
                                                   <option value="4">Apr</option>
                                                   <option value="5">May</option>
                                                   <option value="6">June</option>
                                                   <option value="7">July</option>
                                                   <option value="8">Aug</option>
                                                   <option value="9">Sept</option>
                                                   <option value="10">Oct</option>
                                                   <option value="11">Nov</option>
                                                   <option value="12">Dec</option>
                                                </select>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="field is-horizontal">
                                    <div class="field-label is-normal">
                                       <label class="label">Year</label>
                                    </div>
                                    <div class="field-body">
                                       <div class="field is-narrow">
                                          <div class="control">
                                             <div class="select is-fullwidth">
                                                <select name="by_year" id="by_year">
                                                   <option>Select year</option>
                                                   @for ($i=1997;$i< 2050;$i++)
                                                   <option value="{{$i}}">{{$i}}</option>
                                                   @endfor
                                                </select>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="field is-horizontal">
                                    <div class="field-label">
                                    </div>
                                    <div class="field-body">
                                       <div class="field">
                                          <div class="control">
                                             <button id="by_month_submit" class="button is-primary">
                                             Submit
                                             </button>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div style="display: none;" id="byyear_re">
                                 <div class="field is-horizontal">
                                    <div class="field-label is-normal">
                                       <label class="label">Year</label>
                                    </div>
                                    <div class="field-body">
                                       <div class="field is-narrow">
                                          <div class="control">
                                             <div class="select is-fullwidth">
                                                <select name="only_year" id="only_year">
                                                   <option>Select year</option>
                                                   @for ($i=1997; $i < 2050; $i++)
                                                   <option value="{{$i}}">{{$i}}</option>
                                                   @endfor
                                                </select>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    </div>
                                    <div class="field is-horizontal">
                                       <div class="field-label">
                                       </div>
                                       <div class="field-body">
                                          <div class="field">
                                             <div class="control">
                                                <button id="by_year_submit" class="button is-primary">
                                                Submit
                                                </button>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                           </nav>
                           </div>
                        </div>
                     </div>
               </article>
               </div>
            </div>
            </div>
            </nav>
         </div>
      </div>
      <div class="container" >
         <div class="columns">
            <div class="column" id="showCase" >
      <?php if(count($transactions) != 0){ ?>
               <div class="content">
               	<p class="is-info notification title is-4 has-text-centered">These are the transactions that were added by <strong>You</strong></p>
               </div>
                     <table  class="table  is-fullwidth">
                        <thead>
                           <tr>
                              <th>Date</th>
                              <th>Category</th>
                              <th>Amount</th>
                              <th>Decription</th>
                           </tr>
                        </thead>
                        <tbody id="table_body" >
                           @for ($i=0; $i< count($transactions); $i++)
                           <tr> 
                              <td>{{$transactions[$i]->dateOfEntry}}</td>
                              <td>{{$transactions[$i]->category->category}}</td>
                              <td>{{$transactions[$i]->amount}}</td>
                              <td>{{$transactions[$i]->description}}<span><i class="fa fa-pencil-square-o  is-pulled-right edit "></i></span></td>
                           </tr>
                           @endfor
                        </tbody>
                     </table>
               <div class="content">
                  <?php
                     $sum = 0;
                     for ($i=0; $i< count($amounts); $i++){
                           $sum = $sum +$amounts[$i]->amount;
                     
                     }
                     echo "<h2>Total amount : <b> $sum </b> </h2>"
                      ?>
               </div>
            </div>
         </div>
      </div>
      <?php }else{
        echo '<div class="container" id="msg_notrans">';
            echo '<div class="columns">';
            echo '<div class="column"></div>';
            echo '<div class="column">';
             echo '<div class="box message is-danger has-text-center" >';
             echo '<div class="content" >';
         echo   '<h3>No transactions found.</h3>';
        echo  '</div>';
            echo '</div>';
            echo '</div>';
            echo '<div class="column"></div>';
        
            echo '</div>';
         echo '</div>';
               } ?>
               <div class="modal is_edit">
  <div class="modal-background"></div>
  <div class="modal-content">
    <!-- Any other Bulma elements you want -->
  </div>
  <button class="modal-close is-large" id="close" aria-label="close"></button>
</div>
   </body>
</html>