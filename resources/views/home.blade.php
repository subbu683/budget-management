<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Budma</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="css/bulma.css">
	<link rel="stylesheet" type="text/css" href="css/home.css">
	<script src="js/jquery.js"></script>
	<script src="js/home.js"></script>
</head>
<body>
<div class="intro"></div>
	<div class="bg_image"></div>
<div class="container is-fullhd">
	<div class="columns">
		<div class="column">

			<nav class="navbar  is-light is-transparent " role="navigation" aria-label="main navigation" >
				<div class="navbar-brand">
					<a href="/home" class="nav-item">
						<img src="/storage/images/budma_logo1.png"  height="100" width="132" alt="BUDMA logo">
					</a>

					<div class="burger navbar-burger">
						<span></span>
						<span></span>
						<span></span>
					</div>
				</div>
				<div class="navbar-menu">
				<div class="navbar-start">
				</div>
					<div class="navbar-end">
						<div class="navbar-item">
							

							<div class="field is-grouped">
								<p class="control">
									<a class="button is-primary" href="/getTransactions">
										<span>Get transactions</span>
									</a>
								</p>
								<p class="control">
									<a class="button is-primary" href="/logout">
										<span>logout</span>
									</a>
								</p>
							</div>

						</div>
					</div>
				</div>
			</nav>

		</div>
	</div>
	</div>
		<br>	
		<div class="container ">
			<div class="columns">
				<div class="column"></div>
				<div class="column is-6">			
					@if ($errors->any())
					<article class="message is-danger">
						<div class="message-body has-text-centered">
							@foreach ($errors->all() as $error)
							{{$error}}
							@endforeach
						</div>
							
					</article>
					@endif
					@if(Session::has("inv_user") ||Session::has('inv_cat'))
					<article class="message is-danger">
						<div class="message-body has-text-centered">
							{{Session::get("inv_user")}}
							{{Session::get("inv_cat")}}
						</div>
					</article>
					@endif
					@if(Session::has("trans_success_msg"))
					<article class="message is-success">
						<div class="message-body has-text-centered">
							{{Session::get("trans_success_msg")}}
						</div>
					</article>
					@endif
					@if(Session::has("trans_fail_msg"))
					<article class="message is-warning">
						<div class="message-body">
							{{Session::get("trans_fail_msg")}}
						</div>
					</article>
					@endif
					<div class="box ">
						<form action="" method="POST">
							<div class="card-content">
								<div class="field">
									<label class="label">Amount
										<span class="icon is-small is-left  has-text-danger ">
											<i class="fa fa-question-circle " aria-hidden="true" title="Enter the amount that you spent for something."></i>
										</span> </label>
										<div class="control has-icons-left has-icons-right">
											<input class="input" type="text" name="amount" placeholder="Ex: 1000" required>
											<span class="icon is-small is-left">
												<i class="fa fa-inr"></i>
											</span>
										</div>
									</div>
									<div class="field" >
										<label class="label">Paidby <span class="icon is-small is-left has-text-danger">
											<i class="fa fa-question-circle" aria-hidden="true" title="Give the details about who paid the amount."></i>
										</span></label>
										<div id="checkbox_user">									
											<label class="checkbox" id="check_lable">
												<input type="checkbox" name="paidby_auth" value="{{Auth::id()}}" id="auth_checkbox">
												Me
											</label>
										</div>
										<div class="control" id="auth_hide">
											<div class="select" id="addUser_ajax">
												<select id="user_select" name="paidby" required>
													<option>Select user</option>
													@for($i=0; $i< count($users); $i++)

													<option  value="{{$users[$i]->id}}">{{$users[$i]->username}}</option>

													@endfor

												</select>
											</div>
											<a class="button addUser is-primary is-pulled-right">Add user</a>
										</div>
									</div>
									<div class="field" >
										<label class="label">Category <span class="icon is-small is-left has-text-danger">
											<i class="fa fa-question-circle" aria-hidden="true" title="Category that above amount was spent."></i>
										</span></label>
										<div class="control">
											<div class="select" id="addCategory_ajax">
												<select name="category" required>
													<option>Select Category</option>
													@for($i=0;$i<count($categories);$i++)
													<option value="{{$categories[$i]->id}}">{{$categories[$i]->category}}</option>
													@endfor
												</select>	
											</div>
											<a class="button addCategory is-primary is-pulled-right">Add</a>
										</div>
									</div>							
									<div class="field">
										<label class="label">Date  <span class="icon is-small is-left has-text-danger">
											<i class="fa fa-question-circle" aria-hidden="true" title="Date that the amount was paid."></i>
										</span></label>
										<div class="control">
											<input class="button" name="date" type="date" required>
										</div>
									</div>
									<div class="field">
										<label class="label">Description</label>
										<div class="control">
											<textarea class="textarea" name="description" placeholder="Write something about this transaction"></textarea>
										</div>
									</div>
									<div class="field is-grouped">
										<div class="control">
											<button class="button is-primary">Submit</button>
										</div>
									</div>
									{{csrf_field()}}
									<div class="modal add_category_modal">
										<div class="modal-background"></div>
										<div class="modal-content">
											<article class="message">
												<div class="message-body">
													<div class="field">
														<label class="label">Add category :</label>
														<div class="control">
															<input class="input" id="addCategory" name="addCategory" type="text" placeholder="Ex:Mess bill" >
															<p class="help is-danger" id="addCat_fail"></p>
														</div>
													</div>
													<div class="field">
														<div class="control">
															<a class="button is-primary" id="addButton">Add</a>
															<a class="button is-primary" id="close">Close</a>
														</div>
													</div>
												</div>
											</article>
										</div>
									</div>
									<div class="modal add_user_modal">
										<div class="modal-background"></div>
										<div class="modal-content">
											<article class="message">
												<div class="message-body">
													<div class="field">
														<label class="label">Add user :</label>
														<div class="control">
															<input class="input" id="addUser" name="addUser" type="text" placeholder="Enter name who paid the transaction...." >
															<p class="help is-danger" id="addUser_fail"></p>

														</div>
													</div>
													<div class="field">
														<div class="control">
															<a class="button is-primary" id="addUser_button">Add</a>
															<a class="button is-primary" id="close">Close</a>
														</div>
													</div>
												</div>
											</article>
										</div>
									</div>
								</div>
							</form>
						</div>
					</div>  
					<div class="column">
					</div>  
				</div>
			</div>
		</body>
		</html>
