<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Budma</title>
  <!-- Fonts -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" type="text/css" href="css/bulma.css">
  <link rel="stylesheet" type="text/css" href="css/welcome.css">
  <!-- Styles -->
  <script src="js/jquery.js"></script>
  <script src="js/jquery-ui.min.js"></script>
  <script src="js/welcome.js"></script>
</head>
<body>
<div class="intro"></div>
 
<div class="container is-fullhd">
  <div class="columns">
    <div class="column">

      <nav class="navbar  is-light is-transparent " role="navigation" aria-label="main navigation" >
        <div class="navbar-brand">
          <a href="/home" class="nav-item">
            <img src="/storage/images/budma_logo1.png"  height="100" width="132" alt="BUDMA logo">
          </a>

          <div class="burger navbar-burger">
            <span></span>
            <span></span>
            <span></span>
          </div>
        </div>
        <div class="navbar-menu">
          <div class="navbar-end">
            <div class="navbar-item">
              <div class="field is-grouped">
              <p class="control">
            <a class="is-link is-active button" ><span>
              What's budma?
            </span></a>
                </p>
                <p class="control">
              <a class="button is-primary signin_button" >
                <span>Signin</span>
              </a>
            </p>
               <p class="control ">
              <a class="button is-primary signup_button" >
                <span>Signup</span>
              </a>
            </p>
              </div>

            </div>
          </div>
        </div>
      </nav>

    </div>
  </div>
  </div>





    <div class="container">
      <!-- <div class="notification"> -->

      <div class="columns">
        <div class="column"></div>
        <div class="column is-5">
          @if(Session::has("log_error"))
          {{-- <article class="message is-danger" > --}}
          <span class="tag is-rounded  is-danger is-large">
              {{Session::get("log_error")}}
               <button class="delete "></button>
           </span>

          {{-- </article> --}}
          @endif

        </div>
        <div class="column"></div>

      </div>
    </div>


    <!-- Any other Bulma elements you want -->

    <section class="hero" id="reglog_div" style="display: none;">
      <div class="hero-body">
        <div class="container" >
          <div class="columns">

            <div class="column"></div>
            <div class="column is-5">
             <div class="box" style="">
              <header class="card-header " style="">
                <a class="card-footer-item  title" id="register_card"><b id="signup">Signup</b></a>
              </header>
              <div class="card-content">
                <div class="content">

                  <div class="register" >
                    <form method="post" action="/register">
                      {{csrf_field()}}

                      <div class="field">
                        <div class="control has-icons-left has-icons-right" id="nick_div" >
                          <input class="input" type="text" id="nickname" name="nickname" placeholder="Nickname..." required>
                          <span class="icon is-small is-left" >
                            <i class="fa fa-user" ></i>
                          </span>
                          <span class="icon is-small is-right" >
                            <i class="fa " id="nick_check"></i>
                          </span>
                        </div>
                        <p class="help is-danger" id="nick_success"></p>



                      </div>
                      <div class="field">
                        <div class="control has-icons-left has-icons-right">
                          <input class="input" name="username" id="username" type="text" placeholder="Username..." required>
                          <span class="icon is-small is-left">
                            <i class="fa fa-user"></i>
                          </span>
                          <span class="icon is-small is-right">
                            <i class="fa " id="user_check"></i>
                          </span>
                        </div>
                        <p class="help is-danger" id="user_success"></p>

                      </div>
                      <div class="field">
                        <div class="control has-icons-left has-icons-right">
                          <input class="input" type="password" name="password" id="password" placeholder="Password" required>
                          <span class="icon is-small is-left">
                            <i class="fa fa-lock"></i>
                          </span>
                          <span class="icon is-small is-right">
                            <i class="fa " id="pass_check"></i>
                          </span>
                        </div>
                        <p class="help is-danger" id="pass_success"></p>

                      </div>
                      <div class="field">
                        <label class="label">Date of birth :</label>
                        <div class="control has-icons-left has-icons-right">
                          <input class="button" name="date" id="Date" type="date" required>
                        </div>

                      </div>

                      <div class="field">
                        <div class="control">
                          <button  class="button is-success" id="button_reg">
                            Register
                          </button>
                          <a class="button is-link " id="is-link">Already have account?</a>
                        </div>

                      </div>
                    </form>
                    <div class="control">
                      <button class="button is-link is-pulled-right" id="close">Close</button>
                    </div>
                  </div>
                </div>
              </div>

            </div>


          </div>



          <div class="column"></div>

        </div>
      </div>

    </div>
  </section>

  <section class="hero" id="signin_div" style="display: none;">
    <div class="hero-body">
      <div class="container" >
        <div class="columns">

          <div class="column"></div>
          <div class="column is-5">
            <div class="box" style="">
              <header class="card-header " style="">
                <a class="card-footer-item  title" id="register_card"><b id="signin">Signin</b></a>
              </header>
              <div class="card-content">
                <div class="content">
                  <form action="/login" method="post">
                    <div class="login" id="login_div">
                      <div class="field">
                        {{csrf_field()}}
                        <div class="control has-icons-left has-icons-right">
                          <input class="input" type="text" name="username" placeholder="Username..." required >
                          <span class="icon is-small is-left">
                            <i class="fa fa-user"></i>
                          </span>


                        </div>
                      </div>
                      <div class="field">
                        <div class="control has-icons-left has-icons-right">
                          <input class="input" type="password" name="password" placeholder="Password" required>
                          <span class="icon is-small is-left">
                            <i class="fa fa-lock"></i>
                          </span>

                        </div>

                      </div>
                      <div class="field">
                        <p class="control">
                          <button  class="button is-success">
                            Login
                          </button>

                        </p>
                      </div>
                    </form>
                  </div>
                  <button class="button is-link is-pulled-right" id="Close">Close</button>

                </div>
              </div>

            </div>



          </div>



          <div class="column"></div>

        </div>
      </div>

    </div>
  </section>


  <div class="container details " >
    <div class="columns">
      <div id="text" class="column">
        <section class="hero">
          <div id="text_border" class="hero-body has-text-centered">
            <div class="container">
              <p class="title head is-1">
                Hello Amazing. 
              </p>
              <p class="subtitle subhead is-5">
                Too busy to manage your budgets? We can manage it for you.<br>
                  Budma, a free budget management system.
                </p>
              </div>
            </div>
          </section>    
        </div>

      </div>
    </div>





  </body>
  </html>
