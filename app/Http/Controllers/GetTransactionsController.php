<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Transaction;
use App\User;
use App\Category;
use Response;
use Auth;
use App\Friend;
class GetTransactionsController extends Controller
{    
    public function index()
    {
        $id = Auth::id();
        $transactions =Transaction::where('user_id','=',$id)->with('category','users')->get();
          $categories = Category::where('user_id','=',Auth::id())->get();
        $users = User::where('user_id','=',Auth::id())->get();
        $amounts =Transaction::where('user_id','=',$id)->get();    

        return view('getTransactions',compact('users','categories','transactions','amounts'));
    }
    public function byUser(Request $request)
    { 
        $name = $request->name;
        $transactions = Transaction::where('user_id','=',$name)->get();
        if(count($transactions) > 0)
        {
        $transactions =Transaction::where('user_id','=',$name)->with('category','users')->get();
        if($transactions->isEmpty())
        {
            return;
        }
        else
        {
        return Response::json($transactions);
        }        
        }
        else
        {
            return;
        }
    }
     public function byCategory(Request $request)
    { 
        $name = $request->name;
        $transactions =Transaction::where('category_id','=',$name)->get();
        if(count($transactions) > 0)
        {
        $transactions =Transaction::where('category_id','=',$name)->where('addedby_id','=',Auth::id())->with('users','category')->get();
        if($transactions->isEmpty())
        {
            return;
        }
        else
        {
        return Response::json($transactions);
        }
        }
        else
        {
            return;
        }        
    }
    public function byDate(Request $request)
    {
        $from = $request->from;
        $to = $request->to;        
        $byDateTransactions =Transaction::where('dateOfEntry','>',$from)->where('dateOfEntry','<',$to)->with('users','category')->where('addedby_id','=',Auth::id())->get();
        if($byDateTransactions->isEmpty())
        {
            return;
        }
        else
        {
        return Response::json($byDateTransactions);
        }
    }
     public function byMonth(Request $request)
    {
        $from = $request->from;
        $to = $request->to;        
        $byDateTransactions =Transaction::where('dateOfEntry','>',$from)->where('dateOfEntry','<',$to)->where('addedby_id','=',Auth::id())->with('users','category')->get();
        if($byDateTransactions->isEmpty())
        {
            return;
        }
        else
        {
        return Response::json($byDateTransactions);
        }
    }
     public function byYear(Request $request)
    {
        $from = $request->from;
        $to = $request->to;        
        $byYearTransactions =Transaction::where('dateOfEntry','>',$from)->where('dateOfEntry','<',$to)->where('addedby_id','=',Auth::id())->with('users','category')->get();
        if($byYearTransactions->isEmpty())
        {
            return;
        }
        else
        {
        return Response::json($byYearTransactions);
        }
    }    
}
