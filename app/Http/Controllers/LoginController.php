<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\User;
use Validator;
use Illuminate\Support\Facades\Auth;
use Session;
class LoginController extends Controller
{
    public function login(Request $request)
    {
        $validator = Validator($request->all(),[
            'username' => 'required',
            'password' => 'required',
            ]);
        if($validator->fails())
        {
            return redirect()->back()->withErrors($validator)->withInput();
        }
        else
        {
            $username = $request->username;
            $password = $request->password;
            if(Auth::attempt(['username' => $username,'password' => $password]))
            {
                return redirect('/home');
            }
            else
            {
            Session::flash('log_error','Invalid Username/password combination');
                return redirect()->back();
            }
        }
    }
    public function logout()
    {
        Auth::logout();
        return redirect('/');
    } 
}
