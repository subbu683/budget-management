<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\User;
use Validator;
use Auth;
use Illuminate\Contracts\Auth\Authenticatable;
class RegisterController extends Controller
{
    public function index()
    {
        return view('welcome');
    }
    public function create(Request $request)
    {
        $validator = Validator($request->all(),[
            'nickname' => 'required |max:10|min:2',
            'username' => 'required |unique:users|max:255|min:5',
            'password' => 'required |min:6',
            'date'     => 'required', 
            ]);
        if($validator->fails())
        {
            return redirect()->back()->withError($validator)->withInput();
        }
        else{
            $users = new User();
            $users->nickname = $request->nickname;
            $users->username = $request->username;
            $users->password = bcrypt($request->password);
            $users->birthday = $request->date;
            $users->save();
            return redirect()->back();
        }
    }
}
