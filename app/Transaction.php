<?php
namespace App;
use App\User;
use App\Category;
use Illuminate\Database\Eloquent\Model;
class Transaction extends Model
{
    public function users(){
    	return $this->belongsTo('App\User','user_id');
    }
    public function category(){
    	return $this->belongsTo(Category::class);
    }
}


