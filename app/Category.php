<?php
namespace App;
use App\User;
use App\transaction;
use Illuminate\Database\Eloquent\Model;
class Category extends Model
{
	protected $table ='categories';
    public function transactions()
    {
    	return $this->hasMany(Transaction::class);
    }
}
