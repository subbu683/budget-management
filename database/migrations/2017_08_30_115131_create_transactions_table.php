<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
class CreateTransactionsTable extends Migration
{
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('amount');
            $table->integer('category_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->integer('addedby_id')->unsigned();
            $table->date('dateOfEntry');
            $table->string('description');
            $table->timestamps();
        });
    }
   public function down()
    {
        Schema::dropIfExists('transactions');
    }
}
