<?php
Route::get('/','HomeController@welcome');
Route::get('/home', 'HomeController@index')->middleware('auth');
Route::post('/home','HomeController@store');
Route::get('/getTransactions','GetTransactionsController@index');
Route::post('addCategory', 'AddController@addCategory');
Route::post('addUser', 'AddController@addUser');
Route::post('/register','RegisterController@create');
Route::post('/login','LoginController@login');
Route::get('/logout','LoginController@logout');
Route::post('/getUserTransactions','GetTransactionsController@byUser');
Route::post('/getCategoryTransactions','GetTransactionsController@byCategory');
Route::post('/getBydateTransactions','GetTransactionsController@byDate');
Route::post('/getBymonthTransactions','GetTransactionsController@byMonth');
Route::post('/getByyearTransactions','GetTransactionsController@byYear');
